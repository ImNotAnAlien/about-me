# About me
I'm a programmer and computational physicist (B.S. Physics) who likes math and computers.

## Projects

This is a work in progress of projects I'm moving from a privately hosted gitlab instance to the web so I can show off my work

| Name                                                                                                  | Description                                                                                                    |
|-------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
| [Minecraft CPU project](https://gitlab.com/NathanTwede/perpetual-cpu-minecraft/-/tree/main)          | An 8-bit CPU that was built in Minecraft and programmed to calculate the day of week of a given day/month/year |
| [Zernike polynomial visualizer](https://gitlab.com/NathanTwede/zernike_visualizer)                   | A C++ library and jupyter notebook that calculate and plot 2d zernike polynomials |
| [Runescape Bot overview](https://gitlab.com/NathanTwede/rsbot-doc)                                   | A high level overview/writeup of the progress towards building a runescape bot with Computer Vision and Machine Learning |
| [Raspberry pi slurm cluster](https://gitlab.com/NathanTwede/rpi-cluster-setup-doc)                   | A slurm cluster I'm making out of a few raspberry pi's                                                                   |

## Gallery

Here's various screenshots of projects for those who want a more visual browsing experience:






